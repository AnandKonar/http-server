﻿using System.Diagnostics;
using System.IO;

namespace TestHTTPServer.Server
{
    class CommandExecutor
    {
        private static CommandExecutor _instance;

        public static CommandExecutor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CommandExecutor();
                }
                return _instance;
            }
        }

        private CommandExecutor()
        {
        }

        public byte[] ExecuteGetFunction(string url)
        {
            char[] delimiters = new char[] { '\\', '.' };
            string[] splitedText = url.Split(delimiters);
            string filename = url.Remove(0, 1);
            byte[] result;

            if (splitedText.Length > 0 && splitedText[splitedText.Length - 1] == "php")
            {
                result = GetPHPFile(filename);
            }
            else if (splitedText.Length > 0 && (splitedText[splitedText.Length - 1] == "js" || splitedText[splitedText.Length - 1] == "css" || splitedText[splitedText.Length - 1] == "html"))
            {
                result = GetTextFromFile(filename);
            }
            else
            {
                result = GetBytesFromFile(filename);
            }
            ServerParameters.Instance.WriteToLogFile("Get data from file: " + filename);
            return result;
        }

        private byte[] GetPHPFile(string filename)
        {
            if (ServerParameters.Instance.ActivePHP)
            {
                string phpExePath = @ServerParameters.Instance.PhpPath;
                string filePath = ServerParameters.Instance.SitePath + "//" + filename;
                string fullPath = @Directory.GetCurrentDirectory() + "//" + filePath;

                Process myProcess = new Process();

                // Start a new instance of this program but specify the 'spawned' version. using the PHP.exe file location as the first argument.
                ProcessStartInfo myProcessStartInfo = new ProcessStartInfo(phpExePath, "spawn");
                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;
                myProcessStartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;

                //Provide the other arguments.
                myProcessStartInfo.Arguments = string.Format("{0}", fullPath);
                myProcess.StartInfo = myProcessStartInfo;

                //Execute the process
                myProcess.Start();

                string result = "";
                while (!myProcess.StandardOutput.EndOfStream)
                {
                    string line = myProcess.StandardOutput.ReadLine();
                    result += line;
                }

                return System.Text.Encoding.UTF8.GetBytes(result);
            }
            else
            {
                return GetTextFromFile(filename);
            }
        }

        private byte[] GetBytesFromFile(string filename)
        {
            string filePath = ServerParameters.Instance.SitePath + "//" + filename;
            string fullPath = Directory.GetCurrentDirectory() + "//" + filePath;
            byte[] bytes = new byte[] { };
            if (File.Exists(fullPath))
            {
                bytes = File.ReadAllBytes(fullPath);
            }
            return bytes;
        }

        private byte[] GetTextFromFile(string filename)
        {
            string filePath = ServerParameters.Instance.SitePath + "//" + filename;
            string fullPath = Directory.GetCurrentDirectory() + "//" + filePath;
            string text = "";
            if (File.Exists(fullPath))
            {
                text = File.ReadAllText(fullPath);
            }
            return System.Text.Encoding.UTF8.GetBytes(text);
        }

        public void ExecutePostFunction(string url, string data)
        {

        }
    }
}
