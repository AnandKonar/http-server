﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TestHTTPServer.Server
{
    public class HTTPServer
    {
        private int _port;
        private TcpListener _listener;
        private bool _isActive = true;

        private string _logFile = "";

        public HTTPServer(int port, string logFile = "")
        {
            _port = port;
            _logFile = logFile;
        }

        public void Listen()
        {
            _listener = new TcpListener(_port);
            try
            {
                _listener.Start();
                while (_isActive)
                {

                    TcpClient s = _listener.AcceptTcpClient();
                    HTTPProcessor processor = new HTTPProcessor(s, this);
                    Thread thread = new Thread(new ThreadStart(processor.Process));
                    thread.Start();
                    Thread.Sleep(1);
                }
            }
            catch (Exception e)
            {

            }
        }

        public void GETRequest(HTTPProcessor p)
        {
            byte[] executedFunction = CommandExecutor.Instance.ExecuteGetFunction(p.http_url);
            string contentType = ServerParameters.Instance.GetContentTypeForFile(p.http_url);
            string headers = "HTTP/1.1 200 OK\nContent-Type:" + contentType + ";\nContent-Length: " + executedFunction.Length + "\n\n";
            List<byte> headersBuffer = new List<byte>(Encoding.ASCII.GetBytes(headers));
            headersBuffer.AddRange(executedFunction);
            p.outputStream.Write(headersBuffer.ToArray(), 0, headersBuffer.ToArray().Length);
        }

        public void POSTRequest(HTTPProcessor p, StreamReader inputData)
        {
            string data = inputData.ReadToEnd();
            CommandExecutor.Instance.ExecutePostFunction(p.http_url, data);
        }

        public void StopListening()
        {
            _isActive = false;
            _listener.Stop();
        }

        public void StartListening()
        {
            _listener.Start();
            _isActive = true;
        }
    }
}
