﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using TestHTTPServer.Server;

namespace TestHTTPServer
{
    class ServerParameters
    {
        #region JSONKeys
        private static string _phpExistKey = "ActivePhp";
        private static string _phpPathKey = "PHPPath";
        private static string _portKey = "Port";
        private static string _logFilePathKey = "LogFile";
        private static string _sitePathKey = "Site";
        private static string _headersKey = "Headers";
        #endregion

        #region Parameters
        public bool ActivePHP { get; private set; }
        public string PhpPath { get; private set; }
        public int Port { get; private set; }
        public string LogFile { get; private set; }
        public string SitePath { get; private set; }
        public Dictionary<string, string> ContentTypes { get; private set; }

        private static string _configFile = "server.config.txt";
        private HTTPServer _httpServer;
        private Thread _serverThread;

        private string _currentAdding = "";
        private System.Timers.Timer _timer;
        #endregion

        private Dictionary<string, Action<List<string>>> _consoleFunctions;

        private static ServerParameters _instance;

        public static ServerParameters Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServerParameters();
                }
                return _instance;
            }
        }

        private ServerParameters()
        {
            InitializeParameters();
            InitializeConsoleFunctions();
            _timer = new System.Timers.Timer(500);
            _timer.Elapsed += AppendToFile;
            _timer.Start();
        }

        private void AppendToFile(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (LogFile != "")
            {
                File.AppendAllText(LogFile, _currentAdding);
                _currentAdding = "";
            }
        }

        private void InitializeParameters()
        {
            ActivePHP = false;
            PhpPath = "";
            Port = 8080;
            LogFile = "log.txt";
            SitePath = "site";
            ContentTypes = new Dictionary<string, string>();
        }

        private void InitializeConsoleFunctions()
        {
            _consoleFunctions = new Dictionary<string, Action<List<string>>>();

            _consoleFunctions.Add("start", (parameters) => {
                if (_httpServer == null)
                {
                    _httpServer = new HTTPServer(Port, LogFile);
                    _serverThread = new Thread(new ThreadStart(_httpServer.Listen));
                    _serverThread.Start();
                    string logText = "Server start:";
                    WriteToLogFile(logText);
                }
                else
                {
                    Console.WriteLine("Server already started");
                }
            });

            _consoleFunctions.Add("pause", (parameters) => {
                if (_httpServer != null)
                {
                    _httpServer.StopListening();
                    _serverThread.Suspend();
                }               
                string logText = "Server paused:";
                WriteToLogFile(logText);
            });

            _consoleFunctions.Add("resume", (parameters) => {
                if (_httpServer != null)
                {
                    _httpServer.StartListening();
                    _serverThread.Resume();
                }
                string logText = "Server resumed:";
                WriteToLogFile(logText);
            });

            _consoleFunctions.Add("restart", (parameters) => {
                if (_httpServer != null)
                {
                    _httpServer.StopListening();
                    _httpServer = null;
                }
                _httpServer = new HTTPServer(Port, LogFile);
                _serverThread = new Thread(new ThreadStart(_httpServer.Listen));
                _serverThread.Start();
                string logText = "Server restarted:";
                WriteToLogFile(logText);
            });

            _consoleFunctions.Add("stop", (parameters) => {
                if (_httpServer != null)
                {
                    _httpServer.StopListening();
                    _httpServer = null;
                }
                string logText = "Server stopped:";
                WriteToLogFile(logText);
            });

            _consoleFunctions.Add("exit", (parameters) => {
                if (_httpServer != null)
                {
                    _httpServer.StopListening();
                    _httpServer = null;
                }
                string logText = "Server shutdown:";
                WriteToLogFile(logText);
                Process.GetCurrentProcess().Kill();
            });
        }

        public void WriteToLogFile(string text)
        {
            Console.WriteLine(text + " " + DateTime.Now.ToString());
            _currentAdding += text + " " + DateTime.Now.ToString() + "\n";
        }

        private void ReadParametersFromFile()
        {
            string stringData = File.ReadAllText(_configFile);
            JSONNode parameters = JSON.Parse(stringData);
            if (parameters != null)
            {
                ActivePHP = parameters[_phpExistKey].AsBool;
                PhpPath = parameters[_phpPathKey].Value;
                Port = parameters[_portKey].AsInt;
                LogFile = parameters[_logFilePathKey].Value;
                SitePath = parameters[_sitePathKey].Value;
                ArrayList keys = parameters[_headersKey].GetKeys();
                foreach (string key in keys)
                {
                    ContentTypes.Add(key, parameters[_headersKey][key].Value);
                }
            }
        }

        public string GetContentTypeForFile(string fileUrl)
        {
            char[] delimiters = new char[] { '\\', '.' };
            string[] splitedText = fileUrl.Split(delimiters);

            if (splitedText.Length > 0)
            {
                string findingContentType = "http_content_type_for_" + splitedText[splitedText.Length - 1];
                if (ContentTypes.ContainsKey(findingContentType))
                {
                    return ContentTypes[findingContentType];
                }
            }
            return "text/html";
        }

        public void ExecuteConsoleCommand(string command, List<string> parameters)
        {
            if (_consoleFunctions.ContainsKey(command))
            {
                _consoleFunctions[command].Invoke(parameters);
            }
            else
            {
                WriteToLogFile("Wrong function: " + command);
            }
        }

        static void Main(string[] args)
        {
            ServerParameters.Instance.ReadParametersFromFile();
            while (true)
            {
                string commandFromConsole = Console.ReadLine();
                List<string> commandParameters = new List<string>(commandFromConsole.Split(' '));
                if (commandParameters.Count > 0 && commandParameters[0] != "")
                {
                    string currentCommand = commandParameters[0];
                    commandParameters.RemoveAt(0);
                    ServerParameters.Instance.ExecuteConsoleCommand(currentCommand, commandParameters);
                }
            }
        }
    }
}
